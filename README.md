# What is this

Markov chain bot for all of David Attenborough's narration from both editions of Planet Earth

# Running

Create a `.env` file with: `MASTODON_BASE_URL` and `MASTODON_API_KEY`

## Without docker

Run `pip install -r requirements.txt`.

To make a post:

`python make_toot.py`

## With docker

Run `docker build -t planet_earth_ebooks .`. Then you can have it make a post with `docker run --rm planet_earth_ebooks python make_toot.py`.

If you want it to run on a regular basis, regular old cron is a good option.

FROM python:3-alpine

WORKDIR /kevins_frittatas

COPY requirements.txt ./

RUN apk add --virtual .build-deps gcc musl-dev libffi-dev openssl-dev
RUN pip install -r requirements.txt
RUN apk del --purge .build-deps

COPY . .
